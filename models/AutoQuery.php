<?php

namespace app\models;

use yii\db\Query;

/**
 * This is the ActiveQuery class for [[Auto]].
 *
 * @see Auto
 */
class AutoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Auto[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Auto|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Заполнение таблицы данными из файла /csv.csv
     * Тяжеловато получилось, но для подсобного функционала сойдет
     * @return bool
     * @throws \yii\db\Exception
     */
    public function generate()
    {
        $brands = [];
        if (($handle = fopen(__DIR__ . "/csv.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 25, ";")) !== FALSE) {
                if (count($data) > 1) {
                    if (preg_match('/[^\d]{2}/', $data[0]))
                        $brands[$data[0]][] = $data[1];
                }
            }
            fclose($handle);
        }
        $br = [];
        $row = [];
        $du = [];
        $et = [];
        $autos = [];
        $autoFields = ['brand', 'car_model', 'engine_type', 'drive_unit'];
        foreach ($brands as $key => $val) {
            $br[] = [$key];
        }
        $driveUnits = ['Полный', 'Передний'];
        $engineTypes = ['Бензин', 'Дизель', 'Гибрид'];
        foreach ($driveUnits as $unit) {
            $du[] = [$unit];
        }
        foreach ($engineTypes as $type) {
            $et[] = [$type];
        }
        $transaction = $this->createCommand()->db->beginTransaction();
        try {
            $this->createCommand()->checkIntegrity(false)->execute();
            $this->createCommand()->truncateTable('brand')->execute();
            $this->createCommand()->batchInsert('brand', ['name'], $br)->execute();
            $this->createCommand()->truncateTable('car_model')->execute();
            foreach ($brands as $key => $models) {
                $brand_id = $this->brandId($key);
                $models = array_unique($models);
                foreach ($models as $model) {
                    $row[] = [$model, $brand_id];
                }

                $command = $this->createCommand()->batchInsert('car_model', ['name', 'brand_id'], $row);
                $command->sql = str_replace("INSERT", "INSERT IGNORE", $command->sql);
                $command->execute();
            }
            $this->createCommand()->truncateTable('drive_unit')->execute();
            $this->createCommand()->batchInsert('drive_unit', ['name'], $du)->execute();
            $this->createCommand()->truncateTable('engine_type')->execute();
            $this->createCommand()->batchInsert('engine_type', ['name'], $et)->execute();
            for ($i = 1; $i <= rand(20, 25); $i++) {
                $brand = array_rand($brands);
                $brand_id = $this->brandId($brand);
                $model_id = $this->brandModelId($brand);
                $autos[] = [$brand_id, $model_id, $this->randomId('engine_type'), $this->randomId('drive_unit')];
            }
            $this->createCommand()->truncateTable('auto')->execute();
            $this->createCommand()->batchInsert('auto', $autoFields, $autos)->execute();
            $this->createCommand()->checkIntegrity(true)->execute();
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }

    /**
     * Поличение ID бренда по имени
     * @param string $brand
     * @return int
     */
    private function brandId($brand)
    {
        return (new Query())->select(['id'])->where(['name' => $brand])->from('brand')->one()['id'];

    }

    /**
     * Получение ID моделей бренда по имени бренда
     * @param string $brand
     * @return int
     */
    private function brandModelId($brand)
    {
        $id = $this->brandId($brand);
        $models = (new Query())->select(['id'])->where(['brand_id' => $id])->from('car_model')->all();
        $return = $models[array_rand($models)];
        return !empty($return['id']) ? $return['id'] : 0;
    }

    /**
     * Получение ID-ов записей других таблиц
     * @param string $table
     * @return int
     */
    private function randomId($table)
    {
        $engines = (new Query())->select(['id'])->from($table)->all();
        $return = $engines[array_rand($engines)];
        return !empty($return['id']) ? $return['id'] : 0;
    }

}
