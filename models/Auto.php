<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "auto".
 *
 * @property int $id
 * @property int $brand
 * @property int $car_model
 * @property int $engine_type
 * @property int $drive_unit
 *
 * @property Brand $brand0
 * @property CarModel $carModel
 * @property DriveUnit $driveInit
 * @property EngineType $engineType
 */
class Auto extends \yii\db\ActiveRecord
{
    public $imageFiles;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand', 'car_model', 'engine_type', 'drive_unit'], 'required'],
            [['brand', 'car_model', 'engine_type', 'drive_unit'], 'integer'],
            [['brand'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand' => 'id']],
            [['car_model'], 'exist', 'skipOnError' => true, 'targetClass' => CarModel::className(), 'targetAttribute' => ['car_model' => 'id']],
            [['drive_unit'], 'exist', 'skipOnError' => true, 'targetClass' => DriveUnit::className(), 'targetAttribute' => ['drive_unit' => 'id']],
            [['engine_type'], 'exist', 'skipOnError' => true, 'targetClass' => EngineType::className(), 'targetAttribute' => ['engine_type' => 'id']],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'maxSize' => 1024 * 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand' => 'Brand',
            'car_model' => 'Car Model',
            'engine_type' => 'Engine Type',
            'drive_unit' => 'Drive Init',
        ];
    }

    /**
     * Gets query for [[Brand0]].
     *
     * @return \yii\db\ActiveQuery|BrandQuery
     */
    public function getBrand0()
    {
        return $this->hasOne(Brand::class, ['id' => 'brand']);
    }

    /**
     * Gets query for [[CarModel]].
     *
     * @return \yii\db\ActiveQuery|CarModelQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::class, ['id' => 'car_model']);
    }

    /**
     * Gets query for [[DriveInit]].
     *
     * @return \yii\db\ActiveQuery|DriveUnitQuery
     */
    public function getDriveUnit()
    {
        return $this->hasOne(DriveUnit::class, ['id' => 'drive_unit']);
    }

    /**
     * Gets query for [[EngineType]].
     *
     * @return \yii\db\ActiveQuery|EngineTypeQuery
     */
    public function getEngineType()
    {
        return $this->hasOne(EngineType::class, ['id' => 'engine_type']);
    }

    public function getImages0()
    {
        return $this->hasMany(Images::class, ['auto_id' => 'id']);
    }

    /**
     * Полный список имеющихся в таблице брендов
     * @return array
     */
    public static function getBrandList()
    {
        $data = static::find()->joinWith(['brand0'])
            ->select(['*'])
            ->orderBy('brand.name')->asArray()->all();
        return ArrayHelper::map($data, 'id', 'name');
    }

    /**
     * Список имещихся в таблице моделей для отмеченных брендов
     * @return array
     */
    public static function getModelList()
    {

        $brand = !empty(Yii::$app->request->queryParams['AutoSearch']['brand']) ? Yii::$app->request->queryParams['AutoSearch']['brand'] : null;
        if ($brand) {
            $data = static::find()->joinWith(['carModel'])
                ->select(['*'])
                ->andFilterWhere(['in', 'brand', $brand])
                ->orderBy('car_model.name')->asArray()->all();
        } else {
            $data = static::find()->joinWith(['carModel'])
                ->select(['*'])
                ->orderBy('car_model.name')->asArray()->all();
        }

        return ArrayHelper::map($data, 'id', 'name');
    }

    /**
     * {@inheritdoc}
     * @return AutoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutoQuery(get_called_class());
    }
}
