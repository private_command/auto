<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_model".
 *
 * @property int $id
 * @property string $name
 * @property int $brand_id
 *
 * @property Brand $brand
 * @property DriveUnit[] $driveUnits
 * @property EngineType[] $engineTypes
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'brand_id'], 'required'],
            [['brand_id'], 'integer'],
            [['name'], 'string', 'max' => 10],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'brand_id' => 'Brand ID',
        ];
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery|BrandQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class(), ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[DriveUnits]].
     *
     * @return \yii\db\ActiveQuery|DriveUnitQuery
     */
    public function getDriveUnits()
    {
        return $this->hasMany(DriveUnit::class(), ['model_id' => 'id']);
    }

    /**
     * Gets query for [[EngineTypes]].
     *
     * @return \yii\db\ActiveQuery|EngineTypeQuery
     */
    public function getEngineTypes()
    {
        return $this->hasMany(EngineType::class(), ['model_id' => 'id']);
    }
}
