<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auto;

/**
 * AutoSearch represents the model behind the search form of `app\models\Auto`.
 */
class AutoSearch extends Auto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['brand', 'car_model', 'engine_type', 'drive_unit'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        //Фильтр по id не используется
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        //Фильтры по атрибутам
        $query->andFilterWhere(['in', 'brand', $this->brand]);
        $query->andFilterWhere(['in', 'car_model', $this->car_model]);
        $query->andFilterWhere(['in', 'engine_type', $this->engine_type]);
        $query->andFilterWhere(['in', 'drive_unit', $this->drive_unit]);

        return $dataProvider;
    }
}
