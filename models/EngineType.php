<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "engine_type".
 *
 * @property int $id
 * @property string $name
 * @property int $model_id
 *
 * @property CarModel $model
 */
class EngineType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'engine_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'model_id'], 'required'],
            [['model_id'], 'integer'],
            [['name'], 'string', 'max' => 10],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarModel::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'model_id' => 'Model ID',
        ];
    }

    /**
     * Gets query for [[Model]].
     *
     * @return \yii\db\ActiveQuery|CarModelQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModel::className(), ['id' => 'model_id']);
    }
}
