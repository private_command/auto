<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\EngineType;

/* @var $this yii\web\View */
/* @var $model app\models\AutoSearch */
/* @var $form yii\widgets\ActiveForm */
$url = Url::to('models');
$this->registerJs(<<<JS
//Функция фильтрования
function pjaxSend(){
    var brahds = [];
    var engine = [];
    var drive = [];
    var model = [];
    $.each($('#autosearch-brand input:checked'), function(index,value) {
        brahds.push($(value).val());        
    });
    $.each($('#autosearch-engine_type input:checked'), function(index,value) {
        engine.push($(value).val());        
    });    
    $.each($('#autosearch-drive_unit input:checked'), function(index,value) {
        drive.push($(value).val());        
    });
    $.each($('#SelectModel input:checked'), function(index,value) {
        model.push($(value).val());        
    });
    $.pjax({
        timeout: 4000,
        url: $('#filter-form').attr('action'),
        container: '#list-view',
        fragment: '#list-view',
        data: {AutoSearch: {brand:brahds, engine_type:engine, drive_unit:drive, car_model:model}},
   });
} 
//Фильтр по событию
$(document).on('change', '#filter-form input', function(e) {
    if($(this).hasClass('brand-input')){
        var brahds = [];        
        $.each($('#autosearch-brand input:checked'), function(index,value) {
            brahds.push($(value).val());        
        });
        var mod = [];
        $.each($('#SelectModel input:checked'), function(index,value) {
            mod.push($(value).val());        
        });
        console.log(mod)
        $.post( 
            '/auto/models',
            {id:brahds, models:mod}
        ).done(function( data ) {
            $('#SelectModel').html(data);
            pjaxSend();
        });
    }else {
        pjaxSend();
    }   
});
//сброс фильтра
$(document).on('click', '#reset', function(e) {
    e.preventDefault();
    $.post( 
            '/auto/models',
            {id:{}})
    .done(function( data ) {
            $('#SelectModel').html(data);
            $( "#filter-form input" ).prop( "checked", false );
            $.pjax({
                timeout: 4000,
                url: $('#filter-form').attr('action'),
                container: '#list-view',
                fragment: '#list-view',
                data: {}
            });
    });
});
JS
    , yii\web\View::POS_END);

?>

<div class="auto-search">
    <p>Фильтр</p>
    <?php yii\widgets\Pjax::begin(['id' => 'search']) ?>
    <?php $form = ActiveForm::begin([
        'id' => 'filter-form',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'brand')->checkboxList(
        \app\models\Auto::getBrandList(),
        ['itemOptions' => ['class' => 'brand-input'],
        ]
    )->label('Бренды') ?>

    <?= $form->field($model, 'car_model')->checkboxList(
        \app\models\Auto::getModelList(),
        [
            'prompt' => 'Выберите бренд',
            'id' => 'SelectModel'
        ]
    )->label('Модели') ?>

    <?= $form->field($model, 'engine_type')->checkboxList(
        ArrayHelper::map(EngineType::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name')
    )->label('Двигатель') ?>

    <?= $form->field($model, 'drive_unit')->checkboxList(
        ArrayHelper::map(app\models\DriveUnit::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name')
    )->label('Привод') ?>

    <div class="form-group">
        <?= Html::a('Сброс фильтра', ['#'], ['id' => 'reset', 'class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>

</div>
