<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="news-item span4" style="float: left">
    <div class="card" style="width: 18rem;">
        --------------------------------
        <div class="card-body">
            <h4 class="card-title">Бренд: <?= Html::encode($model->brand0->name) ?></h4>
            <p class="card-text">Модель: <?= Html::encode($model->carModel->name) ?></p>
            <p class="card-text">Двигатель: <?= Html::encode($model->engineType->name) ?></p>
            <p class="card-text">Привод: <?= Html::encode($model->driveUnit->name) ?></p>
        </div>
    </div>
</div>