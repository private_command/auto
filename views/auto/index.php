<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\PjaxAsset;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AutoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//PjaxAsset::register($this);
$this->title = 'Autos';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="auto-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Обновить данные', ['generate'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['id'=>'list-view'],
        'itemOptions' => ['class' => 'item'],
        'layout' => "<div class='lst row'>{items}</div><div>{pager}</div>",
        'itemView' => '_list',
        'pager' => [
            'firstPageLabel' => 'first',
            'lastPageLabel' => 'last',
            'prevPageLabel' => 'previous',
            'nextPageLabel' => 'next',
            'maxButtonCount' => 3,
        ],
    ]) ?>
    <?php Pjax::end(); ?>
</div>
