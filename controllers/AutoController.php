<?php

namespace app\controllers;

use app\models\CarModel;
use Yii;
use app\models\Auto;
use app\models\AutoSearch;
use app\models\AutoQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use bupy7\ajaxfilter\AjaxFilter;

/**
 * AutoController implements the CRUD actions for Auto model.
 */
class AutoController extends Controller
{

    /**
     * Lists all Auto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AutoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Заполнение таблиц новыми данными
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionGenerate()
    {
        $model = new AutoQuery(Auto::class);
        if($model->generate()){
            Yii::$app->session->setFlash('success', 'Данные обновлены.');
        }else{
            Yii::$app->session->setFlash('error', 'Данные не обновлены. Ошибка транзацакции');
        }
        return $this->redirect(['index']);
    }

    /**
     * Набор моделей для отмеченных брендов
     */
    public function actionModels()
    {
        $id = Yii::$app->request->post('id');
        $models = !empty(Yii::$app->request->post('models')) ? Yii::$app->request->post('models') : [];
        $html = '';
        if ($id) {
            $operations = Auto::find()
                ->select(['*', 'car_model.id'])
                ->joinWith(['carModel'])
                ->andFilterWhere(['in', 'brand', $id])
                ->distinct()
                ->all();

        } else {
            $operations = Auto::find()
                ->select(['*', 'car_model.id'])
                ->joinWith(['carModel'])
                ->distinct()
                ->all();
        }
        if ($operations) {
            foreach ($operations as $operation) {
                if(in_array($operation->carModel->id, $models) && $id){
                    $html .= '<label><input type="checkbox" name="AutoSearch[car_model][]" value="' . $operation->carModel->id . '" checked>' . $operation->carModel->name . '</label>';
                }else{
                    $html .= '<label><input type="checkbox" name="AutoSearch[car_model][]" value="' . $operation->carModel->id . '">' . $operation->carModel->name . '</label>';
                }
            }
        }
        return $html;
    }
}
