
Установка
-------------------
Предполагается что git и composer установлены глобально

В каталоге DOCUMENT_ROOT выполнить
````
git clone git@bitbucket.org:private_command/auto.git .
composer install
````
Бекап БД в каталоге /data, импортировать в БД
Подключить базу данных в файле /config/db.php.
Должно заработать

Можно посмотреть в натуре

http://my-test245.000webhostapp.com/

Только фильтр, красивостей нет, 
админки нет, миграций нет. Чистый функционал фильтра.
Кнопка "Обновить данные", загрузит другие данные в количестве 20-25 записей.

Скрипты
-------------------
controllers/AutoController.php

views/auto

models/Auto.php

models/AutoQuery.php

models/AutoSearch.php
